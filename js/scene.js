/*
*
* See Three.js documentation on usage of scene functions
*
* */


var waitTime = 400;
var container = document.getElementById( 'sceneContainer' );
var cameraSettings = {
    width : container.offsetWidth,
    height : container.offsetHeight,
    aspect : container.offsetWidth / container.offsetHeight,
    fov : 50,
    near : .1,
    far : 10000
};
var cameraPosition = {
    x : 0,
    y : 0,
    z : -20
};
var cameraTarget = {
    x : 0,
    y : 0,
    z : 0
};

// Hosted in my github, test ply files
var plyObjectURLs = [
    "https://raw.githubusercontent.com/mitch10e/dight495/gh-pages/ply/helix.ply",
    "https://raw.githubusercontent.com/mitch10e/dight495/gh-pages/ply/dolphins.ply",
    "https://raw.githubusercontent.com/mitch10e/dight495/gh-pages/ply/hind.ply",
    "https://raw.githubusercontent.com/mitch10e/dight495/gh-pages/ply/octahedron.ply",
    "https://raw.githubusercontent.com/mitch10e/dight495/gh-pages/ply/tetrehedron.ply",
    "https://raw.githubusercontent.com/mitch10e/dight495/gh-pages/ply/teapot.ply",
    "https://raw.githubusercontent.com/mitch10e/dight495/gh-pages/ply/galleon.ply",
    "https://raw.githubusercontent.com/mitch10e/dight495/gh-pages/ply/monkey.ply"

];

function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

var plyIndex = getRandomInt(0, 7);
var testPLY = plyObjectURLs[6];

var camera, target, scene, renderer;
var object;
var scale = 0.01;
var MOVE_SPEED = .5;

function init() {
    camera = new THREE.PerspectiveCamera(
        cameraSettings.fov,
        cameraSettings.aspect,
        cameraSettings.near,
        cameraSettings.far
    );
    camera.position.set(
        cameraPosition.x,
        cameraPosition.y,
        cameraPosition.z
    );
    target = new THREE.Vector3(
        cameraTarget.x,
        cameraTarget.y,
        cameraTarget.z
    );
    camera.lookAt(
        target
    );

    scene = new THREE.Scene();
    createLights();
    initRenderer();
    window.addEventListener( 'resize', onWindowResize, false );

}

// Open a PLY file and parse it in to the scene.
function readSingleFile(file) {
    //var file = event.target.files[0];

    if(!file) {
        return;
    }

    var reader = new FileReader();
    reader.onload = function(e) {
        var ply = e.target.result;
        updateProgressBar(50);
        setTimeout(function() {
            loadScene(ply);
        }, waitTime);
    };
    updateProgressBar(25);
    setTimeout(function() {
        reader.readAsText(file);
    }, waitTime / 2);

}


function renderGeometery(geometry) {
    if(object != undefined) {
        scene.remove(object);
    }
    var material = new THREE.MeshPhongMaterial({
        color: COLOR.WHITE,
        specular: 0x123456,
        shininess: 200
    });
    object = new THREE.Mesh(geometry, material);
    object.position.set(0, 0, 0);

    scaleObject(object, scale);

    object.castShadow = true;
    object.receiveShadow = true;

    scene.add( object );
    animate();
}

function loadScene(ply) {
    var loader = new THREE.PLYLoader();
    updateProgressBar(75);
    var geometry;
    setTimeout(function() {
        geometry = loader.parse(ply);
        updateProgressBar(100);
        setTimeout(function() {
            renderGeometery(geometry);
            toggleLoadObjectModal();
            setTimeout(function() {
                $("#drop_zone").toggleClass('greenDotBorder');
                resetProgressBar();
            }, waitTime);
        }, waitTime);
    }, waitTime);
}

// Create Default Lights
function createLights() {
    addAmbientLight(COLOR.DIMWHITE);
    addDirectionalLight(10, 7.5, 0, COLOR.WHITE, 1.35 );
}

// Setting up scene
function initRenderer() {
    renderer = new THREE.WebGLRenderer( { antialias: true } );
    renderer.setPixelRatio( window.devicePixelRatio );
    renderer.setSize(cameraSettings.width, cameraSettings.height);

    renderer.gammaInput = true;
    renderer.gammaOutput = true;

    renderer.shadowMap.enabled = true;
    renderer.shadowMap.cullFace = THREE.CullFaceBack;

    container.appendChild( renderer.domElement );
}

function onWindowResize() {
    camera.updateProjectionMatrix();
    renderer.setSize(container.offsetWidth, container.offsetHeight);
    camera.aspect = container.offsetWidth / container.offsetHeight;
}

// Start rendering
function animate() {
    requestAnimationFrame( animate );
    render();
}

// Rotate object if auto rotate is turned on, then render scene
function render() {
    rotateObject();
    renderer.render( scene, camera );
}

function scaleObject(mesh, scale) {
    mesh.scale.set(scale, scale, scale);
}

// -- Camera

var rotSpeed = 0.025;
var moveSpeed = scale * 5;

function turnLeft() {
    camera.rotation.y -= MOVE_SPEED * rotSpeed;
    updateCameraInfo();
}

function turnRight() {
    camera.rotation.y += MOVE_SPEED * rotSpeed;
    updateCameraInfo();
}

function moveCameraForward() {
    camera.position.x -= moveSpeed * Math.sin((camera.rotation.y));
    camera.position.z += moveSpeed * Math.cos((camera.rotation.y));
    updateCameraInfo();
}

function moveCameraBackward() {
    camera.position.x += moveSpeed * Math.sin((camera.rotation.y));
    camera.position.z -= moveSpeed * Math.cos((camera.rotation.y));
    updateCameraInfo();
}


function moveCameraLeft() {
    camera.position.x -= moveSpeed * Math.sin((camera.rotation.y - (Math.PI / 2)));
    camera.position.z += moveSpeed * Math.cos((camera.rotation.y - (Math.PI / 2)));
    updateCameraInfo();
}

function moveCameraRight() {
    camera.position.x += moveSpeed * Math.sin((camera.rotation.y - (Math.PI / 2)));
    camera.position.z -= moveSpeed * Math.cos((camera.rotation.y - (Math.PI / 2)));
    updateCameraInfo();
}


function moveCameraUp() {
    camera.position.y += moveSpeed;
    updateCameraInfo();
}

function moveCameraDown() {
    camera.position.y -= moveSpeed;
    updateCameraInfo();
}

function resetCameraPosition() {
    camera.position.set(
        cameraPosition.x,
        cameraPosition.y,
        cameraPosition.z
    );
    camera.rotation.set(-Math.PI, 0, -Math.PI);
    updateCameraInfo();
}

function resetObjectOrientation() {
    xRot = 5 * Math.PI / 3.5;
    yRot = 0;
    zRot = Math.PI / 2;
}



// -- Rotation of Object

var isRotatingObjectX = false;
var isRotatingObjectY = false;
var isRotatingObjectZ = false;

var xRot = 5 * Math.PI / 3.5;
var yRot = 0;
var zRot = Math.PI / 2;
var SPEED = 0.25;

var division = 96;

function rotateObjectX() {
    xRot += (Math.PI / division);
    xRot = xRot % (2 * Math.PI);
}

function rotateObjectY() {
    yRot += (Math.PI / division);
    yRot = yRot % (2 * Math.PI);
}

function rotateObjectZ() {
    zRot += (Math.PI / division);
    zRot = zRot % (2 * Math.PI);
}


function rotateObject() {
    var timer = Date.now() * 0.001;
    var xRotation = isRotatingObjectX ? (timer * SPEED) % 2 * Math.PI : xRot;
    var yRotation = isRotatingObjectY ? (timer * SPEED) % 2 * Math.PI : yRot;
    var zRotation = isRotatingObjectZ ? (timer * SPEED) % 2 * Math.PI : zRot;
    object.rotation.set(xRotation, yRotation, zRotation);
    updateObjectInfo();
}

function toggleRotateObjectX() {
    isRotatingObjectX = !isRotatingObjectX;
    $('#rotateXDiv').toggleClass('redHover');
}

function toggleRotateObjectY() {
    isRotatingObjectY = !isRotatingObjectY;
    $('#rotateYDiv').toggleClass('redHover');
}

function toggleRotateObjectZ() {
    isRotatingObjectZ = !isRotatingObjectZ;
    $('#rotateZDiv').toggleClass('redHover');
}







